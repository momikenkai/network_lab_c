#ifndef LAB1_HEADER_H
#define LAB1_HEADER_H

#include <iostream>
#include <map>
#include <ctime>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#ifdef _WIN32

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#endif

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

class IpAddress {
private:
    unsigned short domain;
    const char *ip;
public:
    IpAddress(const char *ip);
    const char *getIpAddress() const;
    unsigned short getDomain() const;
};

class Sockets {
private:
    int socketSendDescriptor;
    int socketRecvDescriptor;
    struct sockaddr_in addressSend;
    struct sockaddr_in6 addressSend6;
public:
    Sockets(IpAddress ipAddress);
    int getSendDescriptor() const;
    int getRecvDescriptor() const;
    struct sockaddr_in getAddressSend() const;
    struct sockaddr_in6 getAddressSend6() const;
};

class CopiesCounter{
private:
    IpAddress ipAddress;
    int socketSendDescriptor;
    int socketRecvDescriptor;
    struct sockaddr_in addressSend;
    struct sockaddr_in6 addressSend6;
    struct sockaddr_in addressRecv;
    struct sockaddr_in6 addressRecv6;
    std::map<char *, time_t> copies;
public:
    CopiesCounter(Sockets sockets, IpAddress ipAddress);
    int count();
    void addAddress(char *address);
    void checking();
};

#endif

