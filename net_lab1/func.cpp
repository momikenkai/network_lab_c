#include "header.h"

IpAddress::IpAddress(const char *ip) {
    domain = AF_INET;
//    if (InetNtop(AF_INET6, ip, nullptr) == 1) {
//        domain = AF_INET6;
//    }
//    else if (net_pton(AF_INET, ip, nullptr) != 1) {
//        throw std::runtime_error("Bad address format.\n");
//    }
    this->ip = ip;
}

unsigned short IpAddress::getDomain() const {
    return domain;
}

const char *IpAddress::getIpAddress() const{
    return ip;
}



Sockets::Sockets(IpAddress ipAddress) {

#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(0x0101, &wsaData)) {
        perror("WSAStartup");
        throw std::runtime_error(":(((\n");
    }
#endif
    socketRecvDescriptor = socket(ipAddress.getDomain(), SOCK_DGRAM, 0);
    if (socketRecvDescriptor < 0) {
        perror("");
        throw std::runtime_error(":(((1\n");
    }
    u_int ok = 1;
    if (setsockopt(socketRecvDescriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&ok, sizeof(ok)) < 0) {
        perror("");
        throw std::runtime_error(":(((2\n");
    }

    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    if(setsockopt(socketRecvDescriptor, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(tv)) < 0) {
        perror("");
        throw std::runtime_error(":(((3\n");
    }

    if(ipAddress.getDomain() == AF_INET) {
        struct sockaddr_in addressRecv;
        memset(&addressRecv, 0, sizeof(addressRecv));
        addressRecv.sin_family = ipAddress.getDomain();
        addressRecv.sin_addr.s_addr = htonl(INADDR_ANY);
        addressRecv.sin_port = htons(1234);
        if (bind(socketRecvDescriptor, (struct sockaddr*) &addressRecv, sizeof(addressRecv)) < 0) {
            perror("");
            throw std::runtime_error(":(((4\n");
        }
    } else{
        struct sockaddr_in6 addressRecv6;
        memset(&addressRecv6, 0, sizeof(addressRecv6));
        addressRecv6.sin6_family = ipAddress.getDomain();
        addressRecv6.sin6_addr = in6addr_any;
        addressRecv6.sin6_port = htons(1234);
        if (bind(socketRecvDescriptor, (struct sockaddr*) &addressRecv6, sizeof(addressRecv6)) < 0) {
            perror("");
            throw std::runtime_error(":(((45\n");
        }
    }

    if(ipAddress.getDomain() == AF_INET) {
        struct ip_mreq mreq;
        mreq.imr_multiaddr.s_addr = inet_addr(ipAddress.getIpAddress());
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);
        if (setsockopt(socketRecvDescriptor, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &mreq, sizeof(mreq)) < 0) {
            perror("");
            throw std::runtime_error(":(((5\n");
        }
    }else{
        struct ipv6_mreq mreq6;
        inet_pton(AF_INET6, ipAddress.getIpAddress(), &mreq6.ipv6mr_multiaddr);
        mreq6.ipv6mr_interface = INADDR_ANY;
        if (setsockopt(socketRecvDescriptor, IPPROTO_IPV6, IP_ADD_MEMBERSHIP, (char *) &mreq6, sizeof(mreq6)) < 0) {
            perror("");
            throw std::runtime_error(":(((56\n");
        }
    }

    socketSendDescriptor = socket(ipAddress.getDomain(), SOCK_DGRAM, 0);
    if (socketSendDescriptor < 0) {
        perror("socket");
        throw std::runtime_error(":(((6\n");
    }
    if(ipAddress.getDomain() == AF_INET) {
        memset(&addressSend, 0, sizeof(addressSend));
        addressSend.sin_family = ipAddress.getDomain();
        addressSend.sin_addr.s_addr = inet_addr(ipAddress.getIpAddress());
        addressSend.sin_port = htons(1234);
    } else {
        memset(&addressSend6, 0, sizeof(addressSend6));
        addressSend6.sin6_family = ipAddress.getDomain();
        inet_pton(AF_INET6, ipAddress.getIpAddress(), (void *)&socket_struct.sin6_addr.s6_addr);
        addressSend6.sin6_port = htons(1234);
    }
}

int Sockets::getSendDescriptor() const {
    return socketSendDescriptor;
}

int Sockets::getRecvDescriptor() const{
    return socketRecvDescriptor;
}

struct sockaddr_in Sockets::getAddressSend() const{
    return addressSend;
}

struct sockaddr_in6 Sockets::getAddressSend6() const{
    return addressSend6;
}

CopiesCounter::CopiesCounter(Sockets sockets, IpAddress ipAddress) : ipAddress(ipAddress){
    socketSendDescriptor = sockets.getSendDescriptor();
    socketRecvDescriptor = sockets.getRecvDescriptor();
    if (ipAddress.getDomain() == AF_INET){
        addressSend = sockets.getAddressSend();
    } else {
        addressSend6 = sockets.getAddressSend6();
    }

}

int CopiesCounter::count() {
    const char *sendMessage = "Hi!";
    char recvMessage[2];
    int n;
    char *ip = (char *)malloc(30);
    if(ipAddress.getDomain() == AF_INET) {
        n = sendto(socketSendDescriptor, sendMessage, strlen(sendMessage), 0, (struct sockaddr *) &addressSend,
                   sizeof(addressSend));
    } else{
        n = sendto(socketSendDescriptor, sendMessage, strlen(sendMessage), 0, (struct sockaddr *) &addressSend6,
                   sizeof(addressSend6));
    }
    if (n < 0) {
        perror("sendto");
        throw std::runtime_error(":(((7\n");
    }
    while(true){
        int len, rc;
        if(ipAddress.getDomain() == AF_INET) {
            len = sizeof(addressRecv);
            rc = recvfrom(socketRecvDescriptor, recvMessage, 2, 0, (struct sockaddr *) &addressRecv, &len);
        } else {
            len = sizeof(addressRecv6);
            rc = recvfrom(socketRecvDescriptor, recvMessage, 2, 0, (struct sockaddr *) &addressRecv6, &len);
        }
        if (rc < 0) {
            perror("recvfrom");
            throw std::runtime_error(":(((8\n");
        } else if (rc == EWOULDBLOCK){
            if(ipAddress.getDomain() == AF_INET) {
                n = sendto(socketSendDescriptor, sendMessage, strlen(sendMessage), 0, (struct sockaddr *) &addressSend,
                           sizeof(addressSend));
            } else{
                n = sendto(socketSendDescriptor, sendMessage, strlen(sendMessage), 0, (struct sockaddr *) &addressSend6,
                           sizeof(addressSend6));
            }
            if (n < 0) {
                perror("sendto");
                throw std::runtime_error(":(((78\n");
            }
            checking();
        } else {
            if(ipAddress.getDomain() == AF_INET) {
                inet_ntop(AF_INET, &(addressRecv.sin_addr), ip, INET_ADDRSTRLEN);
                addAddress(ip);
            } else {
                inet_ntop(AF_INET6, &(addressRecv6.sin6_addr), ip, INET6_ADDRSTRLEN);
                addAddress(ip);
            }
            checking();
        }


    }
}

void CopiesCounter::addAddress(char *address){
    if(copies.find(address) == copies.end()) {
        time_t sec = time(NULL);
        copies.insert(std::pair<char  *, time_t>(address, sec));
    }
    std::cout << address << std::endl;
}

void CopiesCounter::checking(){
    time_t sec = time(NULL);
    for(auto ip: copies){
        if(ip.second - sec > 6){
            copies.erase(ip.first);
        } else {
            std::cout << std::endl;
            std::cout << ip.first;
        }
    }
}























