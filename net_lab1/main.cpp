#include "header.h"

int main(int argc, char *argv[]) {
    try {
        IpAddress ipAddress(argv[1]);
        Sockets sockets(ipAddress);
        CopiesCounter copiesCounter(sockets, ipAddress);
        copiesCounter.count();
    }
    catch (std::runtime_error &name) {
        std::cout << name.what();
    }
    return 0;
}